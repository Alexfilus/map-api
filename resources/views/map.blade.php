<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Управление отходами</title>

    <!-- Fonts -->
{{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&subset=cyrillic,cyrillic-ext"
          rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="{{ URL::asset('js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/map.js') }}" type="text/javascript"></script>
    <style>
        body, html {
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
            font-family: 'Roboto';
        }

        #map {
            width: 600px;
            max-width: 99%;
            height: 600px;
        }
    </style>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            /*font-family: 'Raleway', sans-serif;*/
            /*font-weight: 100;*/
            height: 100vh;
            margin: 0;
        }


        .title {
            font-size: 3em;
            margin: 20px auto;
        }
        .title > img {
            vertical-align: middle;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col s12">
            <div class="title m-b-md">
                <img src="https://utilexpert.ru/wp/wp-content/uploads/2016/12/logo_new_2.png" alt="logo" width="258"
                     height="73"> &nbsp;&nbsp;
                Калькулятор расстояний
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m6">
            <div class="row">
                <div class="col s12">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="row">
                <form class="col s12" method="post" action="/api/getCoords/">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="point0" name="points[0]" value="Ростов кременчугская 1">
                            <label for="point0">Точка А</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <input type="text" id="latitude0" value="" disabled>
                            <label for="latitude0">Широта</label>
                        </div>
                        <div class="col s6">
                            <input type="text" id="longitude0" value="" disabled>
                            <label for="longitude0">Долгота</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="point1" name="points[1]" value="Ростов самоходный 16">
                            <label for="point1">Точка Б</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <input type="text" id="latitude1" value="" disabled>
                            <label for="latitude1">Широта</label>
                        </div>
                        <div class="col s6">
                            <input type="text" id="longitude1" value="" disabled>
                            <label for="longitude1">Долгота</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <input type="text" id="distance" value="" disabled>
                            <label for="distance">Расстояние по прямой</label>
                        </div>
                        <div class="col s6">
                            <input type="text" id="route_length" value="" disabled>
                            <label for="route_length">Длина маршрута</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light">
                                посчитать
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
