<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Geotools\Coordinate\Ellipsoid;
use Toin0u\Geotools\Facade\Geotools;
use GuzzleHttp\Client;
use \Yandex\Geo\Api as YandexApi;

class YandexController extends Controller
{
    public function getCoords(Request $request)
    {
        $arPoints = $request->input('points');
        $result = [];
        foreach ($arPoints as $sPoint) {
            $arPoint = $this->requestCoords($sPoint);
            $result[] = $arPoint;
        }
        return $result;
    }

    private function requestCoords(String $address)
    {
        $api = new YandexApi();
        $api->setQuery($address)->setLimit(1)->load();
        $response = $api->getResponse();
        $item = $response->getFirst();
        return [
            'address' => $item->getAddress(),
            'latitude' => $item->getLatitude(),
            'longitude' => $item->getLongitude(),
        ];
    }

    public function getDistance(Request $request)
    {
        $arPoints = $request->input('points');
        $geotools = new \League\Geotools\Geotools();
        $coordA = new \League\Geotools\Coordinate\Coordinate($arPoints[0]);
        $coordB = new \League\Geotools\Coordinate\Coordinate($arPoints[1]);
        $distance = $geotools->distance()->setFrom($coordA)->setTo($coordB);
        return $distance->in('m')->haversine();
    }

    public function getRouteLength(Request $request)
    {
        $arPoints = $request->input('points');
        $url = "http://maps.googleapis.com/maps/api/directions/json?origin={$arPoints[0][0]},{$arPoints[0][1]}&destination={$arPoints[1][0]},{$arPoints[1][1]}";
        //$response = self::sendRequest($url);

        $client = new Client();
        $response = $client->get($url)->getBody();
        $arResponse = json_decode($response, true);

        return $arResponse['routes'][0]['legs'][0]['distance']['value'];
    }

    private static function sendRequest($url)
    {
        $client = new Client();
        $res = $client->request('GET', $url, []);
        return json_decode($res->getBody());
    }
}
