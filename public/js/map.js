var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);

function init() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    myMap = new ymaps.Map('map', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [55.76, 37.64], // Москва
        zoom: 10
    }, {
        searchControlProvider: 'yandex#search'
    });
}

$(document).ready(function () {
    $('form').submit(function (e) {
        var action = e.currentTarget.action;
        $.getJSON(action, $(this).serialize(), function (data) {
                console.log(data);
                var arAddr = [];
                var points = [];
                $(data).each(function (index, value) {
                    arAddr.push(value.address);
                    points.push([value.latitude, value.longitude]);
                    $('#point' + index).val(value.address);
                    $('#latitude' + index).val(value.latitude);
                    $('#longitude' + index).val(value.longitude);
                });
                ymaps.route(arAddr, {
                    mapStateAutoApply: true,
                    avoidTrafficJams: true
                }).then(function (route) {
                    route.getPaths().options.set({
                        // в балуне выводим только информацию о времени движения с учетом пробок
                        balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass('$[properties.humanJamsTime]'),
                        // можно выставить настройки графики маршруту
                        strokeColor: '0000ffff',
                        opacity: 0.9
                    });
                    //$('#route_length').val(route.getLength());
                    // добавляем маршрут на карту
                    myMap.geoObjects.add(route);
                });
                $.getJSON('/api/getDistance', {points: points}, function (data) {
                    console.log(data);
                    $('#distance').val(data);
                });
                $.getJSON('/api/getRouteLength', {points: points}, function (data) {
                    console.log(data);
                    $('#route_length').val(data);
                });
            }
        );
        return false;
    });
});